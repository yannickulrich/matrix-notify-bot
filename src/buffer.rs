#[derive(Debug)]
pub struct CBuffer<T> {
    write_index : usize,
    size : usize,
    buffer : Vec<T>
}

pub struct CBufferIntoIterator<'a, T> {
    content: &'a CBuffer<T>,
    index: usize,
    nret : usize
}

impl<T> CBuffer<T> {
    pub fn with_capacity(size : usize) -> CBuffer<T> {
        CBuffer {
            size: size,
            write_index: 0,
            buffer: Vec::<T>::with_capacity(size)
        }
    }

    pub fn push(&mut self, el : T) {
        if self.buffer.len() <= self.write_index {
            self.buffer.push(el);
        } else {
            self.buffer[self.write_index] = el;
        }
        self.write_index += 1;
        self.write_index %= self.size;
    }
}

impl<'a, T> IntoIterator for &'a CBuffer<T> {
    type Item = &'a T;
    type IntoIter = CBufferIntoIterator<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        CBufferIntoIterator{
            content: self,
            nret: 0,
            index: if self.size > self.buffer.len() {
                    // Buffer isn't fully used yet
                    0
                } else {
                    self.write_index
                }
        }
    }
}

impl<'a, T> Iterator for CBufferIntoIterator<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<&'a T> {
        if self.nret >= self.content.size {
            None
        } else if self.nret >= self.content.buffer.len() {
            None
        } else {
            self.index %= self.content.size;
            let ans = Some(&self.content.buffer[self.index]);
            self.nret += 1;
            self.index += 1;
            ans
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_finite() {
        let mut buf = CBuffer::<String>::with_capacity(10);
        buf.push("Hello".to_string());
        buf.push("World".to_string());
        assert_eq!(buf.buffer.len(), 2);
    }

    #[test]
    fn add_many() {
        let mut buf = CBuffer::<String>::with_capacity(4);
        buf.push("Hello1".to_string());
        buf.push("World1".to_string());
        buf.push("Hello2".to_string());
        buf.push("World2".to_string());
        buf.push("Hello3".to_string());
        buf.push("World3".to_string());
        assert_eq!(buf.buffer.len(), 4);
    }

    #[test]
    fn iter_small() {
        let mut buf = CBuffer::<&str>::with_capacity(4);
        buf.push("Hello1");
        buf.push("World1");

        let mut it = buf.into_iter();
        assert_eq!(it.next(), Some(&"Hello1"));
        assert_eq!(it.next(), Some(&"World1"));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn iter_large() {
        let mut buf = CBuffer::<&str>::with_capacity(4);
        buf.push("Hello1");
        buf.push("World1");
        buf.push("Hello2");
        buf.push("World2");
        buf.push("Hello3");
        buf.push("World3");

        let mut it = buf.into_iter();
        assert_eq!(it.next(), Some(&"Hello2"));
        assert_eq!(it.next(), Some(&"World2"));
        assert_eq!(it.next(), Some(&"Hello3"));
        assert_eq!(it.next(), Some(&"World3"));
        assert_eq!(it.next(), None);
    }
}
