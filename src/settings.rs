use config::{Config, ConfigError, File};
use clap::ArgMatches;
use serde_derive::Deserialize;
use std::path::PathBuf;
use matrix_sdk::{
    ruma::{
        OwnedUserId, OwnedDeviceId, OwnedRoomId,
    }
};
use rand::thread_rng;
use rand::seq::SliceRandom;

#[derive(Debug, Deserialize)]
#[allow(unused,non_camel_case_types)]
pub enum MatrixAuth {
    token,
    password
}

#[derive(Debug, Deserialize)]
#[allow(unused,non_camel_case_types)]
pub struct MatrixToken {
    pub token : String,
    pub deviceid : OwnedDeviceId,
}

#[derive(Debug, Deserialize)]
#[allow(unused,non_camel_case_types)]
pub struct Matrix {
    pub homeserver : String,
    pub user : OwnedUserId,
    pub auth : MatrixAuth,

    pub password : Option<String>,
    pub token : Option<MatrixToken>,
    pub roomid : OwnedRoomId
}

#[derive(Debug, Deserialize,Clone,Copy)]
#[allow(unused,non_camel_case_types)]
pub enum Strategy {
    stdbuf,
    script
}
impl Default for Strategy {
    fn default() -> Self { Strategy::stdbuf }
}

#[derive(Debug, Deserialize,Clone,Copy)]
#[allow(unused,non_camel_case_types)]
pub enum StdbufStrategy {
    none,
    line,
    default
}
impl Default for StdbufStrategy {
    fn default() -> Self { StdbufStrategy::line }
}


#[derive(Debug, Deserialize,Clone)]
#[allow(unused,non_camel_case_types)]
pub struct Stdbuf {
    pub libstdbuf : Option<PathBuf>,
    pub stdout : StdbufStrategy,
    pub stderr : StdbufStrategy
}
impl Default for Stdbuf {
    fn default() -> Self {  Stdbuf {
        libstdbuf : Some(
            PathBuf::from("/usr/libexec/coreutils/libstdbuf.so")
        ),
        stdout : StdbufStrategy::default,
        stderr : StdbufStrategy::default
    }}
}

#[derive(Debug, Deserialize,Clone)]
#[allow(unused,non_camel_case_types)]
pub struct Buffering {
    pub scrollbackbuffer : Option<usize>,
    pub linebuffer : Option<usize>,
    pub strategy : Strategy,
    pub stdbuf : Option<Stdbuf>
}
impl Default for Buffering {
    fn default() -> Self {  Buffering {
        scrollbackbuffer : Some(10),
        linebuffer : Some(1000),
        strategy : Strategy::default(),
        stdbuf : Some(Stdbuf::default())
    }}
}

#[derive(Debug,Deserialize,Clone)]
#[allow(unused,non_camel_case_types)]
pub struct Wordlist(Vec<String>);
impl Wordlist {
    pub fn get(&self) -> String {
        let mut rng = thread_rng();
        self.0.choose(&mut rng).unwrap().clone()
    }
}

impl Default for Wordlist {
    fn default() -> Self {
        let words = vec![
            "time", "year", "people", "way", "day", "thing", "life",
            "child", "world", "school", "state", "family", "group",
            "hand", "part", "place", "case", "week", "system", "work",
            "number", "night", "point", "home", "water", "room",
            "area", "money", "story", "fact", "month", "lot", "right",
            "study", "book", "eye", "job", "word", "issue", "side",
            "kind", "head", "house", "friend", "power", "hour",
            "game", "line", "end", "member", "law", "car", "city",
            "name", "team", "minute", "idea", "kid", "body", "back",
            "parent", "face", "others", "level", "office", "door",
            "health", "art", "war", "party", "result", "change",
            "reason", "moment", "air", "force"
        ];

        Wordlist(
            words.iter().map(|c| c.to_string()).collect::<Vec<String>>()
        )
    }
}

#[derive(Debug, Deserialize,Clone,Copy)]
#[allow(unused,non_camel_case_types)]
pub struct ColourScheme {
    pub regular : [u32;8],
    pub bright  : [u32;8]
}
impl Default for ColourScheme {
    fn default() -> Self {
        ColourScheme {
            regular : [
                0x2e3436,
                0xcc0000,
                0x4e9a06,
                0xc4a000,
                0x3465a4,
                0x75507b,
                0x06989a,
                0xd3d7cf
            ],
            bright : [
                0x555753,
                0xef2929,
                0x8ae234,
                0xfce94f,
                0x729fcf,
                0xad7fa8,
                0x34e2e2,
                0xeeeeec
            ]
        }
    }
}

#[derive(Debug, Deserialize)]
#[allow(unused,non_camel_case_types)]
pub struct Settings {
    pub matrix : Matrix,
    pub buffering : Option<Buffering>,
    pub wordlist : Option<Wordlist>,
    pub colour : Option<ColourScheme>
}

impl Settings {
    pub fn new(name : Option<PathBuf>, res : &ArgMatches) -> Result<Self, ConfigError> {
        let mut options : Vec<Option<PathBuf>> = vec![
            name,
            Some(PathBuf::from("config.yaml")),
            Some(PathBuf::from("config.toml"))
        ];

        match dirs::home_dir() {
            Some(home) => {
                let mut h = home.clone();
                h.push(".notify.yaml");
                options.push(Some(h));

                let mut h = home.clone();
                h.push(".notify.toml");
                options.push(Some(h));
            }
            None => {}
        };

        match dirs::config_dir() {
            Some(conf) => {
                let mut c = conf.clone();
                c.push(".notify.yaml");
                options.push(Some(c));

                let mut c = conf.clone();
                c.push(".notify.toml");
                options.push(Some(c));
            }
            None => {}
        };

        let good_name : PathBuf = options.iter().filter_map(
            |c| match c {
                Some(c) if c.exists() => Some(c),
                _ => None
            }).next().unwrap().clone();

        let s = Config::builder()
            .add_source(File::from(good_name))
            .set_override_option(
                "matrix.roomid",
                match res.get_one::<String>("room") {
                    Some(x) => Some(x.clone()),
                    _ => None
                }
            )?
            .set_override_option(
                "matrix.auth",
                match res.get_one::<String>("auth") {
                    Some(x) => Some(x.clone()),
                    _ => None
                }
            )?
            .set_override_option(
                "matrix.token.token",
                match res.get_one::<String>("token") {
                    Some(x) => Some(x.clone()),
                    _ => None
                }
            )?
            .set_override_option(
                "buffering.strategy",
                match res.get_one::<String>("strategy") {
                    Some(x) => Some(x.clone()),
                    _ => None
                }
            )?
            .set_override_option(
                "buffering.scrollbackbuffer",
                match res.get_one::<usize>("bufsize") {
                    Some(&x) => Some(x as u64),
                    _ => None
                }
            )?
            .set_override_option(
                "buffering.linebuffer",
                match res.get_one::<usize>("linesize") {
                    Some(&x) => Some(x as u64),
                    _ => None
                }
            )?
            .build()?;
        s.try_deserialize()
    }

}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn load() {
        // let m : ArgMatches = Default::default();
        // let settings = Settings::new(Some(PathBuf::from("tests/config.min.yaml")), &m);


        // println!("{:?}", settings);

    }

}
