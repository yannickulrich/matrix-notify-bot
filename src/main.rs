mod format;
mod buffer;
mod settings;
use tracing::{info, warn, error};
use tracing_unwrap::OptionExt;
use std::path::PathBuf;
use clap::{
    Command as CC,
    Arg,
    value_parser,
    ArgAction
};
use tokio::io::{BufReader, AsyncBufReadExt};
use tokio::process::{Child, Command};
use std::sync::{Arc, Mutex};
use std::time::Instant;

use std::process::Stdio;
use url::Url;
use matrix_sdk::{
    config::SyncSettings,
    room::Room,
    room::Joined,
    event_handler::Ctx,
    ruma::{
        events::room::{
            message::{
                MessageType,
                OriginalSyncRoomMessageEvent,
                RoomMessageEventContent,
                TextMessageEventContent,
            },
        }
    },
    Client, Session,
};

type MutBuffer = Arc<std::sync::Mutex<buffer::CBuffer<String>>>;

async fn on_room_message(event: OriginalSyncRoomMessageEvent, room: Room, user_data : Ctx<(MutBuffer, String, Instant, settings::ColourScheme)>) {
    let (buffer, myname, t0, colours) = user_data.0;

    if let Room::Joined(room) = room {
        let msg_body = match event.content.msgtype {
            MessageType::Text(TextMessageEventContent { body, .. }) => body,
            _ => return,
        };
        let mut msg_it = msg_body.split(" ");

        let cmd = msg_it.next();
        info!("Received message {} (cmd={:?})", msg_body, cmd);

        match cmd {
            Some("cat") if msg_it.next() == Some(&myname) => {
                let body_plain : String = {
                    let buffer = buffer.lock().unwrap();
                    buffer.into_iter()
                        .map(|c| c.clone())
                        .collect::<Vec<String>>()
                        .join("\n")
                };
                info!("Sending {} bytes", body_plain.len());
                room.send(
                    format::ansi2msg(body_plain, colours),
                    None
                ).await.unwrap();
            },

            Some("status") => {
                let duration = t0.elapsed().as_secs();
                let body_plain = format!(
                    "{} is running for {}:{:<02}",
                    myname,
                    duration / 60,
                    duration % 60
                );
                let body = format!(
                    "<code>{}</code> is running for {}:{:<02}",
                    myname,
                    duration / 60,
                    duration % 60
                );
                room.send(
                    RoomMessageEventContent::text_html(body_plain, body),
                    None
                ).await.unwrap();
            },

            _ => {return}
        }
    }
}


async fn login_and_sync1(settings : &settings::Matrix, colours : settings::ColourScheme, name : String, buffer : &MutBuffer, t0 : Instant) -> anyhow::Result<(Client, Joined, String)> {

    let client : Client = match &settings.auth {
        settings::MatrixAuth::password => {
            info!("Using password-based login");
            let password = settings.password.as_ref().expect_or_log(
                "Need to specify password when using password login"
            );
            #[allow(unused_mut)]
            let mut client_builder = Client::builder().homeserver_url(&settings.homeserver);
            let client = client_builder.build() .await?;
            let response = client
                .login_username(&settings.user, &password)
                .initial_device_display_name("command bot")
                .send().await?;

            warn!("First login! Store the access token and devicce ID");
            warn!(
                "token: {}, deviceid: {}",
                response.access_token, response.device_id
            );
            client
        },
        settings::MatrixAuth::token => {
            info!("Using token-based login");
            let token = &settings.token.as_ref().expect_or_log(
                "Need to specify toke when using toke login"
            );
            let client = Client::new(Url::parse(&settings.homeserver)?).await?;

            let session = Session {
                access_token: token.token.clone(),
                refresh_token: None,
                user_id: settings.user.clone(),
                device_id: token.deviceid.clone(),
            };

            client.restore_login(session).await?;
            client
        }
    };

    let sync_token = client
        .sync_once(SyncSettings::default())
        .await
        .unwrap()
        .next_batch;

    info!("Loging & sync successful");

    client.add_event_handler_context((buffer.clone(), name, t0, colours));
    client.add_event_handler(on_room_message);

    let room = client.get_joined_room(&settings.roomid).unwrap();
    Ok((client, room, sync_token))
}

async fn iowatcher<T>(stdout : T, buf : &MutBuffer, maxline : usize) -> anyhow::Result<()>
where
    T : tokio::io::AsyncRead + Unpin
{
    let mut reader = BufReader::new(stdout).lines();
    while let Some(line) = reader.next_line().await? {
        println!("{}", line);

        {
            let mut mline = line;
            if mline.len() > maxline {
                mline.truncate(maxline);
                mline.push_str(" (truncated)");
            }
            let mut buf = buf.lock().unwrap();
            buf.push(mline);
        }
    }
    info!("I/O watcher finished");

    Ok(())
}

async fn run_command(settings : settings::Settings, child : &mut Child, name : &String, cstr : &String) -> anyhow::Result<()> {
    let t0 = Instant::now();

    let default = settings::Buffering::default();

    let linebuf_size = settings.buffering
      .clone()
      .unwrap_or_default()
      .linebuffer
      .unwrap_or(default.linebuffer.unwrap());
    let buffer : MutBuffer = Arc::new(Mutex::new(
        buffer::CBuffer::<String>::with_capacity(
            settings.buffering
              .unwrap_or_default()
              .scrollbackbuffer
              .unwrap_or(default.scrollbackbuffer.unwrap())
        )
    ));
    let stdout = child.stdout.take()
        .expect_or_log("child did not have a handle to stdout");
    let stderr = child.stderr.take()
        .expect_or_log("child did not have a handle to stderr");

    let (client, room, token) = login_and_sync1(&settings.matrix, settings.colour.unwrap_or_default(), name.clone(), &buffer, t0).await?;

    let body_plain = format!( "Starting {} as {}", cstr, name);
    let body = format!( "Starting <code>{}</code> as <code>{}</code>", cstr, name);
    room.send(
        RoomMessageEventContent::text_html(body_plain, body),
        None
    ).await.unwrap();

    let settings = SyncSettings::default().token(token);

    let sync_op = client.sync(settings);
    let stdout_op = iowatcher(stdout, &buffer, linebuf_size);
    let stderr_op = iowatcher(stderr, &buffer, linebuf_size);
    let child_op = child.wait();

    let msg = tokio::select! {
        v = child_op => {
            match v {
                Ok(v) => {
                    info!("Job finished with exit code {:?}", v);
                    match v.code() {
                        Some(v) => format!(" with exit code {}", v),
                        None => String::from(" with signal")
                    }
                },
                e => {
                    error!("Job finished with error {:?}", e);
                    String::from(" with some strange error")
                }
            }
        },
        e = sync_op => {
            error!("matrix sync ended prematurely {:?}", e);
            child.wait().await.unwrap();
            String::from(": matrix sync ended prematurely")
        },
        Err(e) = stdout_op => {
            error!("stdout worker crashed {:?}", e);
            child.wait().await.unwrap();
            String::from(": stdout worker crashed")
        },
        Err(e) = stderr_op => {
            error!("stderr worker crashed {:?}", e);
            child.wait().await.unwrap();
            String::from(": stderr worker crashed")
        },
    };
    let duration = t0.elapsed().as_secs();
    let body_plain = format!(
        "{} finished after {}:{:<02}{}",
        name,
        duration / 60,
        duration % 60,
        msg
    );
    let body = format!(
        "<code>{}</code> finished after {}:{:<02}{}",
        name,
        duration / 60,
        duration % 60,
        msg,
    );
    room.send(
        RoomMessageEventContent::text_html(body_plain, body),
        None
    ).await.unwrap();

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let res = CC::new("notify")
        .arg(Arg::new("loglevel")
             .long("loglevel")
             .action(ArgAction::Set)
             .value_name("LOG-LEVEL")
             .num_args(1)
             .default_value("WARN")
             .help("Sets the loglevel")
             .value_parser(value_parser!(tracing::Level)))
        .arg(Arg::new("config")
             .short('c')
             .long("config")
             .action(ArgAction::Set)
             .value_name("FILE")
             .num_args(1)
             .help("Provides a config file")
             .value_parser(value_parser!(PathBuf)))
        .arg(Arg::new("room")
             .short('r')
             .long("room")
             .action(ArgAction::Set)
             .value_name("!room@server.tdl")
             .num_args(1)
             .value_parser(clap::value_parser!(String))
             .help("The room ID to message"))
        .arg(Arg::new("auth")
             .short('a')
             .long("auth")
             .action(ArgAction::Set)
             .num_args(1)
             .value_parser(["token", "password"])
             .help("whether to use password or token authentication"))
        .arg(Arg::new("token")
             .short('t')
             .long("token")
             .action(ArgAction::Set)
             .value_name("auth token")
             .num_args(1)
             .value_parser(clap::value_parser!(String))
             .help("The access token to use"))
        .arg(Arg::new("strategy")
             .short('s')
             .long("strategy")
             .action(ArgAction::Set)
             .value_name("buffering strategy")
             .num_args(1)
             .value_parser(["script", "stdbuf"])
             .help("Which buffering strategy to use"))
        .arg(Arg::new("name")
             .short('n')
             .long("name")
             .action(ArgAction::Set)
             .value_name("JOB_NAME")
             .num_args(1)
             .help("The name to use for this job"))
        .arg(Arg::new("linesize")
             .long("linesize")
             .action(ArgAction::Set)
             .num_args(1)
             .value_parser(value_parser!(usize))
             .value_name("NBYTES")
             .help("The maximal line length"))
        .arg(Arg::new("bufsize")
             .short('l')
             .long("bufsize")
             .action(ArgAction::Set)
             .num_args(1)
             .value_parser(value_parser!(usize))
             .value_name("NLINES")
             .help("The size of the scrollback buffer"))
        .arg(Arg::new("args")
             .required(true)
             .num_args(1..)
             .trailing_var_arg(true))
        .get_matches();

    tracing_subscriber::fmt()
        .with_max_level(*res.get_one::<tracing::Level>("loglevel").unwrap())
        .with_target(false)
        .init();

    info!("Loaded arguments {:#?}", res);

    let settings = settings::Settings::new(res.get_one::<PathBuf>("config").cloned(), &res)?;
    info!("Loaded settings {:#?}", settings);

    let name : String =
        if let Some(name) = res.get_one::<String>("name") {
            name.to_string()
        } else {
            settings.wordlist.clone().unwrap_or_default().get()
        };

    let cstr = res
        .get_many::<String>("args")
        .unwrap()
        .map(|s| s.as_str())
        .fold(String::new(), |a, b| a + b + " ");

    let mut args = res
        .get_many::<String>("args")
        .unwrap()
        .map(|s| s.as_str());

    let mut stdin = None;
    let buffering = settings.buffering.clone().unwrap_or_default();
    let mut child : Child = match buffering.strategy {
        settings::Strategy::stdbuf => {
            let settings = buffering.stdbuf.unwrap_or_default();

            let mut cmd = Command::new(args.next().unwrap());
            cmd
                .args(args)
                .stdout(Stdio::piped())
                .stderr(Stdio::piped())
                .env("LD_PRELOAD", settings.libstdbuf.unwrap_or(
                    PathBuf::from(
                        "/usr/libexec/coreutils/libstdbuf.so"
                    )
                ));

            match settings.stdout {
                settings::StdbufStrategy::none => {cmd.env("_STDBUF_O", "U");},
                settings::StdbufStrategy::line => {cmd.env("_STDBUF_O", "L");},
                settings::StdbufStrategy::default => {}
            }
            match settings.stderr {
                settings::StdbufStrategy::none => {cmd.env("_STDBUF_E", "U");},
                settings::StdbufStrategy::line => {cmd.env("_STDBUF_E", "L");},
                settings::StdbufStrategy::default => {}
            }

            cmd.spawn()?
        },
        settings::Strategy::script => {
            let shell = std::env::var("SHELL")
                .unwrap_or(String::from("/bin/bash"));

            #[cfg(any(target_os = "linux", target_os = "android"))] {
                let arg = args.fold(String::new(), |a, b| a + b + " ");
                let mut cmd = Command::new("script");
                cmd.args(&["-qec", &arg, "/dev/null"])
                    .env("SHELL", shell)
                    .stdin(Stdio::piped())
                    .stdout(Stdio::piped())
                    .stderr(Stdio::piped());
                let mut child = cmd.spawn()?;
                stdin = Some(child.stdin.take()
                    .expect_or_log("child did not have a handle to stdin"));
                child
            }
            #[cfg(any(target_os = "macos", target_os = "freebsd"))] {
                let arg = args.fold(String::new(), |a, b| a + b + " ");
                let mut cmd = Command::new("script");
                cmd.args(&["-q", "/dev/null", shell, "-c", &arg])
                    .stdin(Stdio::piped())
                   .stdout(Stdio::piped())
                   .stderr(Stdio::piped());
                cmd.spawn()?
            }
        }
    };


    run_command(settings, &mut child, &name, &cstr).await?;

    if let Some(_stdin) = stdin {
    }

    Ok(())
}
