use tracing::{error};
use crate::settings;
// Modified from https://docs.rs/ansi-to-html
use matrix_sdk::ruma::events::room::message::RoomMessageEventContent;

#[derive(PartialEq,Eq,Copy,Clone,Debug)]
enum State {
    Font(u32),
    Bold,
    Italic,
    Strike,
    Underline
}

struct Stack{
    me : Vec<State>
}
impl Stack {
    pub fn new() -> Self {
        Stack {
            me : vec![]
        }
    }

    pub fn push(&mut self, el : State) -> String {
        self.me.push(el);
        match el {
            State::Bold => String::from("<b>"),
            State::Italic => String::from("<i>"),
            State::Underline => String::from("<u>"),
            State::Strike => String::from("<strike>"),
            State::Font(c) => {
                format!("<font color=\"#{:06x}\">", c)
            },
        }
    }
    pub fn pop(&mut self, el : State) -> String {
        let mut ans = String::new();
        let mut buf : Vec<State> = vec![];
        while let Some(i) = self.me.pop() {
            ans.push_str(match i {
                State::Bold => "</b>",
                State::Italic => "</i>",
                State::Underline => "</u>",
                State::Strike => "</strike>",
                State::Font(_) => "</font>",
            });

            // Font patterns don't have to match perfectly
            if let State::Font(_) = i {
                if el == State::Font(u32::MAX) {
                    break;
                }
            }
            if i == el {
                break;
            }
            buf.push(i);
        }

        for i in buf.iter().rev() {
            ans.push_str(&self.push(*i));
        }
        ans
    }
}

#[derive(PartialEq,Eq,Copy,Clone,Debug)]
enum Mode {
    Normal,
    Colour,
    Colour8BitExec,
    Colour24BitR,
    Colour24BitG,
    Colour24BitB,
}

pub fn ansi2html(body_plain : String, colours : settings::ColourScheme) -> Result<(String, String), String> {
    let mut stack : Stack = Stack::new();

    let mut it = body_plain.chars();

    let mut plain = String::new();
    let mut html = String::from("<pre>");
    let mut mode = Mode::Normal;

    while let Some(byte) = it.next() {
        if byte == '\x1b' {
            if it.next() != Some('[') {
                continue;
            }

            let mut num = String::new();
            let mut fullcolour : u32 = 0;
            while let Some(n) = it.next() {
                if n == '0' && mode == Mode::Normal {
                    continue;
                } else if n == ';' || n == 'm' {
                    match mode {
                        Mode::Colour => {
                            match num.as_str() {
                                "5" => { mode = Mode::Colour8BitExec; },
                                "2" => { mode = Mode::Colour24BitR; },
                                _ => {
                                    return Err(format!("invalid 38-colour {}", num));
                                }
                            }
                        },
                        Mode::Colour8BitExec => {
                            let col = match num.parse::<u8>() {
                                Ok(c) => c,
                                Err(e) => {
                                    return Err(format!("parsing of 8bit colour failed: {}", e));
                                }
                            };
                            if col < 8 {
                                html.push_str(&stack.push(State::Font(
                                    colours.regular[col as usize]
                                )));
                            } else if col < 16 {
                                html.push_str(&stack.push(State::Font(
                                    colours.bright[(col-8) as usize]
                                )));
                            } else if col < 232 {
                                // col = 16 + 36 * r + 6 * g + b (0 <= r, g, b <= 5)
                                let red   = ((col-16) / 36      ) as u32;
                                let green = (((col-16) / 6) % 6 ) as u32;
                                let blue  = ((col-16) % 6       ) as u32;
                                html.push_str(&stack.push(State::Font(
                                    ((if red > 0 { 40*red + 55 } else { 0 }) << 16)
                                    | ((if green > 0 { 40*green + 55 } else { 0 }) << 8)
                                    | (if blue > 0 { 40*blue + 55 } else { 0 } )
                                )));
                            } else {
                                let level = ((col - 232) * 10 + 8) as u32;
                                html.push_str(&stack.push(State::Font(
                                    level * 0x010101
                                )));
                            }
                            mode = Mode::Normal;
                        },
                        Mode::Colour24BitR => {
                            match num.parse::<u32>() {
                                Ok(c) => { fullcolour = c << 16; },
                                Err(e) => {
                                    return Err(format!("parsing of red component failed: {}", e));
                                }
                            }
                            mode = Mode::Colour24BitG;
                        },
                        Mode::Colour24BitG => {
                            match num.parse::<u32>() {
                                Ok(c) => { fullcolour |= c << 8; },
                                Err(e) => {
                                    return Err(format!("parsing of green component failed: {}", e));
                                }
                            }
                            mode = Mode::Colour24BitB;
                        },
                        Mode::Colour24BitB => {
                            match num.parse::<u32>() {
                                Ok(c) => { fullcolour |= c; },
                                Err(e) => {
                                    return Err(format!("parsing of blue component failed: {}", e));
                                }
                            }
                            html.push_str(&stack.push(State::Font(fullcolour)));
                            mode = Mode::Normal;
                        },
                        Mode::Normal => {
                            match num.as_str() {
                                "" => {
                                    while stack.me.len() > 0 {
                                        html.push_str(&stack.pop(stack.me[stack.me.len()-1]));
                                    }
                                },
                                "1" => {
                                    html.push_str(&stack.push(State::Bold));
                                },
                                "22" => {
                                    html.push_str(&stack.pop(State::Bold));
                                },
                                "3" => {
                                    html.push_str(&stack.push(State::Italic));
                                },
                                "23" => {
                                    html.push_str(&stack.pop(State::Italic));
                                },
                                "4" => {
                                    html.push_str(&stack.push(State::Underline));
                                },
                                "24" => {
                                    html.push_str(&stack.pop(State::Underline));
                                },
                                "9" => {
                                    html.push_str(&stack.push(State::Strike));
                                },
                                "29" => {
                                    html.push_str(&stack.pop(State::Strike));
                                },
                                "39" => {
                                    html.push_str(&stack.pop(State::Font(u32::MAX)));
                                },
                                "38" => {
                                    mode = Mode::Colour;
                                },
                                x if x.chars().nth(0) == Some('3') => {
                                    html.push_str(&stack.push(State::Font(
                                        colours.regular[((x.chars().nth(1).unwrap() as u8) - ('0' as u8)) as usize]
                                    )));
                                },
                                x if x.chars().nth(0) == Some('9') => {
                                    html.push_str(&stack.push(State::Font(
                                        colours.bright[((x.chars().nth(1).unwrap() as u8) - ('0' as u8)) as usize]
                                    )));
                                },
                                _ => {
                                    return Err(format!("unknown sequence {}", num));
                                }
                            }
                        }
                    }
                    num = String::new();
                    if n == 'm' { break; }
                    continue;
                } else {
                    num.push(n);
                }
            }
        } else {
            html.push(byte);
            plain.push(byte);
        }
    }

    html.push_str("</pre>");
    Ok((plain, html))
}

pub fn ansi2msg(body : String, colours : settings::ColourScheme) -> RoomMessageEventContent {
    match ansi2html(body, colours) {
        Ok((plain, html)) => RoomMessageEventContent::text_html(plain, html),
        Err(error) => {
            error!("ANSI parsing failed {}", error);
            RoomMessageEventContent::text_plain(format!("ANSI parsing failed {}", error))
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn simple() {
        let (_, html) = ansi2html(String::from("Hello"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre>Hello</pre>");

        let (_, html) = ansi2html(String::from("Hello\nWorld"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre>Hello\nWorld</pre>");
    }

    #[test]
    fn simpleformat() {
        let (_, html) = ansi2html(String::from("\x1b[1mbold\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><b>bold</b></pre>");
        let (_, html) = ansi2html(String::from("\x1b[1mbold\x1b[22m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><b>bold</b></pre>");

        let (_, html) = ansi2html(String::from("\x1b[4mbold\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><u>bold</u></pre>");
        let (_, html) = ansi2html(String::from("\x1b[4mbold\x1b[24m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><u>bold</u></pre>");

        let (_, html) = ansi2html(String::from("\x1b[3mbold\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><i>bold</i></pre>");
        let (_, html) = ansi2html(String::from("\x1b[3mbold\x1b[23m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><i>bold</i></pre>");
    }

    #[test]
    fn propernested() {
        let (_, html) = ansi2html(String::from("\x1b[1mTEST\x1b[4mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><b>TEST<u>TEST</u></b></pre>");

        let (_, html) = ansi2html(String::from("\x1b[1mTEST\x1b[4mTEST\x1b[24mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><b>TEST<u>TEST</u>TEST</b></pre>");

        let (_, html) = ansi2html(String::from("\x1b[1;4mTEST\x1b[24mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><b><u>TEST</u>TEST</b></pre>");

        let (_, html) = ansi2html(String::from("\x1b[1;4mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><b><u>TEST</u></b></pre>");
    }

    #[test]
    fn inpropernested() {
        let (_, html) = ansi2html(String::from("\x1b[1mTEST\x1b[4mTEST\x1b[22mTEST\x1b[24m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><b>TEST<u>TEST</u></b><u>TEST</u></pre>");
    }

    #[test]
    fn coloursimple() {
        let (_, html) = ansi2html(String::from("\x1b[31mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><font color=\"#cc0000\">TEST</font></pre>");
    }

    #[test]
    fn colour8bit() {
        let (_, html) = ansi2html(String::from("\x1b[38;5;3mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><font color=\"#c4a000\">TEST</font></pre>");
        let (_, html) = ansi2html(String::from("\x1b[38;5;20mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><font color=\"#0000d7\">TEST</font></pre>");
        let (_, html) = ansi2html(String::from("\x1b[38;5;135mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><font color=\"#af5fff\">TEST</font></pre>");
        let (_, html) = ansi2html(String::from("\x1b[38;5;243mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><font color=\"#767676\">TEST</font></pre>");
    }

    #[test]
    fn colour24bit() {
        let (_, html) = ansi2html(String::from("\x1b[38;2;115;75;234mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><font color=\"#734bea\">TEST</font></pre>");
        let (_, html) = ansi2html(String::from("\x1b[38;2;224;53;78mTEST\x1b[0m"), settings::ColourScheme::default()).expect("");
        assert_eq!(html, "<pre><font color=\"#e0354e\">TEST</font></pre>");
    }

    #[test]
    fn realworld() {
        let inp = "\x1b[0m\x1b[01;34mbuild\x1b[0m
\x1b[01;34mdeps\x1b[0m
\x1b[01;34mexamples\x1b[0m
\x1b[01;34mincremental\x1b[0m
\x1b[01;32mnotify\x1b[0m
notify.d\n";
        let ans = "<pre><b><font color=\"#3465a4\">build</font></b>
<b><font color=\"#3465a4\">deps</font></b>
<b><font color=\"#3465a4\">examples</font></b>
<b><font color=\"#3465a4\">incremental</font></b>
<b><font color=\"#4e9a06\">notify</font></b>
notify.d
</pre>";
        let (_, html) = ansi2html(String::from(inp), settings::ColourScheme::default()).expect("");
        assert_eq!(html, ans);
    }
}
