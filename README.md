# Notify bot

A matrix bot that wraps around shell commands and tells you when they
are done.
```shell
$ notify /path/to/long/running/job arg1 arg2 ...
```

## Features
 * notifications on job completion, incl. runtime and exit code
 * run as many jobs as you want
 * view job's current output (stdout and stderr)
 * disable buffering using `stdbuf`
 * fake tty using `script` (optional)
 * support for some commonly used ANSI SGR sequences (#5)
 * configurable in yaml and toml (looks for files `./config.yaml`, `~/.notify.yaml`, `$XDG_CONFIG_HOME/notify.yaml` or the same files with `.toml`)
 * self-hostable: works on any matrix instance you can create an account

## Setup
 1. Create an account for the bot on the matrix instance of your choice
 2. Create a room between the bot's account and your own
 3. Create a config file
```yaml
---
matrix:
  homeserver: https://matrix.org          # the bot's home server
  user: '@jobbot:matrix.org'              # the bot's user name
  auth: password
  password: correct-horse-battery-staple  # the bot's password
  roomid: "!XXXXXXXXX:matrix.org"         # the ID of the room to work in
```
 4. Run `notify`
 5. (recommended) on first login, the bot produces a device ID and access token.
    You might want to use this for further authentication
```yaml
---
matrix:
  ...
  auth: token
  token:
    token: syt_XXXXXXXX_XXXXXXXXXXXXXXXXXXXX_XXXXXX
    deviceid: XXXXXXXXXX
```

## Configuration
Have a look at `conf.example.yaml`
